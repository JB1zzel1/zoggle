#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<conio.h>
#include <time.h>
#include <sound.h>
#include <graphics.h>


//app stuff
#define APP_INFO "Zoggle by JBB"
#define APP_NAME "Zoggle"
#define APP_KEY  'Z'
#define APP_TYPE AT_Bad

struct window mine; /* Window structure */

/*
To Do
-----

-ensure score input is appropreate
-no backspace?
*/

int roundScore1, roundScore2, Score2, Score1;  
char Name1[10]; 
char Name2[10];
//method
void DrawBoard(unsigned char Hands[]);
void timer();
void noises();
void gosound();
void flush();
void drawlogo();
void scoring();
void namescore();

//vars
int main () {
   int i, n, Di, seed, win;
   //int Score1, Score2;
   time_t t;
  // int roundScore1, roundScore2;   ZOGGLE == 12345 
//char DiceFaces[96] = {'T','T','R','E','L','Y','N','H','N','L','Z','R','L','R','E','I','X','D','M','U','O','C','T','I','O','W','T','O','A','T','T','O','E','S','I','S','B','B','A','O','O','J','E','E','G','N','A','A','E','E','U','S','N','I','C','S','O','A','P','H','S','P','F','A','F','K','Y','R','D','V','L','E','U','N','H','I','M','Q','T','S','T','I','Y','D','E','R','W','T','H','V','G','E','W','N','H','E'}; 
//char DiceFaces[96] = {'t','t','r','e','l','y','n','h','n','l','Z','r','l','r','e','i','x','d','m','u','O','c','t','i','o','w','t','o','a','t','t','o','e','s','i','s','b','b','a','o','o','j','e','e','G','n','a','a','e','e','u','s','n','i','c','s','o','a','p','h','s','p','f','a','f','k','y','r','d','v','L','e','u','n','h','i','m','q','t','s','t','i','y','d','e','r','w','t','h','v','G','E','w','n','h','e'}; 
char DiceFaces[96] = {'T','T','R','6','L','Y','N','H','N','L','1','R','L','R','E','I','X','D','M','U','2','C','T','I','O','W','T','O','A','T','T','O','E','S','I','S','B','B','A','O','O','J','E','E','3','N','A','A','E','E','U','S','N','I','C','S','O','A','P','H','S','P','F','A','F','K','Y','R','D','V','5','E','U','N','H','I','M','Q','T','S','T','I','Y','D','E','R','W','T','H','V','4','E','W','N','H','E'}; 

unsigned char Hands[16];
char store, store2;
//char Name1[10]; 
//char Name2[10];


clrscr();
drawlogo();
printf("%c[1m               Zoggle  \n \n", 27);
printf("%c[2m   The 3 minute word search game\n",27);
printf("      J Bradbury 2021 - z88DK.\n");
printf("    www.AllMyLinks.com\\Z88EPROM\n");
noises();
printf("\n           Press any key!\n");
getchar();
flush();

clrscr();
printf("Player 1 , what is your  name? ");
scanf("%s", &Name1);
//gotoxy(1,1); - this works!!!!
printf("Player 2 , what is your  name? ");
scanf("%s", &Name2);

Score1 = 0;
Score2 = 0;
win = 0;
//while the game is not won, play the game..
while(win < 1){  


n = 17;
Di = 0;
   /* Intializes random number generator */
   srand((unsigned) time(&t));
  
   /* pick 16 of the 96 faces */
      for( i = 1 ; i < n ; i++ ) {
         Hands[i] = DiceFaces[Di + rand() % 6];
         Di=(Di+6);
      }

/* NOW RANDOMLY ORDER THE HAND
*/
n=31;
for( i = 1 ; i < n ; i++ ) {
      seed = rand() % 15 + 1;
      store = Hands[16];
      store2 = Hands[seed];
      Hands[16] = store2;
      Hands[seed] = store;
      }

roundScore1 = 0;
roundScore2 = 0;

clrscr();
DrawBoard(Hands);
namescore();
drawlogo();
timer();
clrscr();
gotoxy(24,3);
printf("Times UP!");
noises();
gotoxy(23,4);
printf("Press a key");
sleep(2);
getchar();
flush();

// Enter scores
DrawBoard(Hands);
scoring();
printf("%s score? ", Name1);
sleep(2);
scanf("%d", &roundScore1);
DrawBoard(Hands);
scoring();
printf("%s score ", Name2);
sleep(2);
scanf("%d", &roundScore2);

Score1 += roundScore1;
Score2 = Score2 + roundScore2;

//has anyone won the game?
if(Score1 > 99){
   if(Score1 > Score2){
        clrscr();
         printf("\n       %s wins!! %d        %d \n", Name1, Score1, Score2);
         win = 1;
         noises();
         sleep(2);
         getchar();
         getk();
   }
}
if(Score2 > 99){
   if(Score2 > Score1){
     clrscr();
      printf("\n       %s wins!! %d    Vs.    %d \n", Name2, Score1, Score2);
      win = 1;
      noises();   
      sleep(2);
      getchar();
      getk();

   }
}

if (win < 1){
   clrscr();
   printf("             GET READY FOR THE NEXT ROUND!! \n \n");
   printf("                       Press a key");
   sleep(2);
   getchar();
   flush();
}

}

printf("To play again, type RUN and press enter");

return(0);
}

void noises()
{
   int i;

   for (i=0; i < 8 ; i++ ) {
      //printf("bit_fx(%u)\n",i);
      bit_fx(i);
   }
   //other sound effects that could be used
   /*for (i=0; i < 8 ; i++ ) {
      printf("bit_fx2(%u)\n",i);
      bit_fx2(i);
   }
   for (i=0; i < 8 ; i++ ) {
      printf("bit_fx3(%u)\n",i);
      bit_fx3(i);
   }
   for (i=0; i < 8 ; i++ ) {
      printf("bit_fx4(%u)\n",i);
      bit_fx4(i);
   }
   for (i=0; i < 8 ; i++ ) {
      printf("bit_fx5(%u)\n",i);
      bit_fx5(i);
   } */
return(0);
}

void gosound()
{
      bit_fx2(2);
   
return(0);
}

void flush(){
// maybe a loop here that checks the input buffer and does getk() untill empty?
getk();
return(0);
}

void scoring(){
gotoxy(0,2); 
printf("Letters: 3 4 5 6 7 8+");
gotoxy(0,3); 
printf("Points : 1 1 2 3 5 11");
gotoxy(0,5); 

return(0);
}

void namescore(){
gotoxy(0,1); 
printf("%c[1mScores%c[2m",27,27);
gotoxy(0,2); 
printf("%s:%d", Name1, Score1);
gotoxy(0,3); 
printf("%s:%d", Name2, Score2); 

return(0);
}

void timer() {
   int i,x,y;
plot(0,45);
drawr(160,0);
   x=0;
   y=51;
   gotoxy(46,6);
   printf("%c[1mTime%c[2m",27,27);
   gosound();
for( i = 1 ; i < 21 ; i++ ){
   sleep(9);
    plot(x, y);
    //x H, y V?
    drawr(4, 4);
    drawr(4, -4);
    drawr(-4, -4);
    drawr(-4, 4);
   flush();
   x=x+8;
}

   return(0);
}

void DrawBoard(unsigned char Hands[]){
clrscr();
int n,i,j,k;
n=5;
k=0;
i=1;
for( j = 1 ; j < 5 ; j++ ){
   gotoxy(23,k);
      for( i; i < n ; i++ ) {
         if (Hands[i] == 'Q'){printf("Qu");}
         else if (Hands[i] == '1'){printf("%c[1mZ %c[2m",27,27);}
         else if (Hands[i] == '2'){printf("%c[1mO %c[2m",27,27);}
         else if (Hands[i] == '3'){printf("%c[1mG %c[2m",27,27);}
         else if (Hands[i] == '4'){printf("%c[1mG %c[2m",27,27);}
         else if (Hands[i] == '5'){printf("%c[1mL %c[2m",27,27);}
         else if (Hands[i] == '6'){printf("%c[1mE %c[2m",27,27);}
         else{printf("%c ",Hands[i]);}
         }
k=k+2;
n=n+4;
 }     
return (0);
}

void drawlogo()
{
    int i,y;

    mine.graph = 1;
    mine.width = 255;
    mine.number = '4';

    /* Open map with width 256 on window #4 */
    window(&mine);

    /* Clear the graphics window */
    clg();  

i= 15;
y=15;

plot(i, y+2);
/* Draw a Z */
    //x H, y V?
    drawr(20, 0);
    drawr(-20, 20);
    drawr(20, 0);

 plot(i+20, y+12);

  /* Draw a O */
    //x H, y V?
    drawr(10, 10);
    drawr(10, -10);
    drawr(-10, -10);
    drawr(-10, 10);

     plot(i+58, y+12);

  /* Draw a G */
    //x H, y V?
    drawr(5, 0);
    drawr(-10, 10);
    drawr(-10, -10);
    drawr(10, -10);
    drawr(5, 5);

     plot(i+80, y+12);

  /* Draw a G */
    //x H, y V?
    drawr(5, 0);
    drawr(-10, 10);
    drawr(-10, -10);
    drawr(10, -10);
    drawr(5, 5);

 plot(i+89, y+2);

  /* Draw a L */
    //x H, y V?
    drawr(0, 20);
    drawr(20, 0);

 plot(i+106, y+12);

  /* Draw a E */
    //x H, y V?
    drawr(20, 10);
    plot(i+106, y+12);
    drawr(20, 0);
    plot(i+106, y+12);
    drawr(20, -10); //was 10
    /* Close the graphics window */
    closegfx(&mine);

return(0);

  }