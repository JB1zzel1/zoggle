# Zoggle

Zoggle is the Boggle dice game for the Cambridge Computer Z88. Implemented in C using Z88DK
2021 - J Bradbury

ZOGGLE presentation:

https://youtu.be/_1W5QdL43KE



https://allmylinks.com/z88eprom

## Name
Zoggle for the Z88

## Using Emulation

OZVM is the best emulator option, although it doesn't have sound, and Zoggle has some pretty jazzy sound effects...

https://cambridgez88.jira.com/wiki/spaces/OZVM/overview

## On real hardware

Use Easy Link 2 on the PC, to transfer Zoggle to the Z88 via a serial cable.

Software here:

https://sourceforge.net/projects/z88/files/EazyLink%20Desktop%20Client/


## Description / Rules

Zoggle - By J Bradbury, for Z88, 2021.

The classic 3 minute word game!

Object of the Game

Find as many words as you can in the 4x4 letter grid. 


To win the match, be the player with the highest score over 100. Or practice by yourself to beat your personal best.

Game Play
Players sit around the table so that everyone can see the Zoggle grid when it's in play. Each player will need a pencil and paper (not included).
When the timer starts, each player searches the assortment of letters for words of three letters or more. When a player finds a word, he/she writes it down.
Words are formed from adjoining letters. Letters must join in the proper sequence to spell a word. They may join horizontally, vertically, or diagonally, to the left, right, or up-and-down.
No letter cube, however, may be used more than once within a single word.

Finding Words
Search the assortment of letters for words of three or more letters.
Words are formed from letters that adjoin in sequence horizontally, vertically or diagonally in any direction. No letter may be used more than once within a single word. When you find a word, write it down. Keep looking and writing until time's up.

Bonus Letters
If there's a BOLD letter in your grid, you'll score double for words you make that include this letter.

Types of Words Allowed
Any words (noun, verb, adjective, adverb, etc)., plural of, form of, or tense is acceptable as long as it can be found in a standard English dictionary.
Words within words are also allowed: spare, spa, par, are, spar, pare. Proper nouns (Smith, Ohio, France, etc). are not allowed.

End of the Game
When time runs out, the timer light will stop flashing red and make a soft beep. All players must stop writing. Each player in turn then reads his or her list aloud.

Important: Any word that appears on more than one player's list must be crossed off all lists - and no one gets credit for it!

Players score their remaining words as follows:

| Score |  |
| ------ | ------ |
| NO. OF LETTERS   | 3 4 5 6 7 8 or more |
| POINTS | 1 1 2 3 5 11 |

     

            
Double the score for any words that included a bonus letter!
The winner is the player who earned the most points when play stopped who is the first player to reach100 points.


## Visuals
see files

## Installation
Compile using... https://z88dk.org/site/

Compile with the following instruction....

+z88 -clib=ansi Zoggle01.c

## Usage
Play Zoggle on a train!

The classic 3 minute word game

## Support
https://z88dk.org/site/

## Roadmap
This is very much a work in progress,
Maybe adding some graphics would be nice,
a play agaisnt the computer mode? maybe.

## Authors and acknowledgment

Thanks to:

G Strube

Dom 

https://z88dk.org/site/

## License
Ask

## Project status
WIP

